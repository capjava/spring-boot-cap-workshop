Welcome to Spring boot workshop. 

Preparations.
Run DB script to create database versatileteacher db in src/main/sql folder.
db/jpa configurations can be found in application.properties



These are step instructions of how to set up and develop  a simple spring boot web application with storing users. 
1)
Create Spring boot application class in package folder spring.boot.cap.workshop
The classs will be used as a configuration class and use this annotation.
public class Application{]

Add static void main args method with Application Spring Run class
Add necessary annotations
public static void main(String[] args) {
    ApplicationContext ctx = SpringApplication.run(Application.class, args);
}     

2)
we are going to read context data from application.properties. 
Add property resource annotation and refer to this file application.properties in src/main/resources

3)
We are going to add spring workshop mvc config.
And the class will be called MvcConfig

add method

 @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("home");
    }
    
4)
we are going to add a root controller class with all mappings of mvc post get requests
Call the class RootController.
we want to inject the class with one mailservice and one user service

 @Autowired
  public RootController(MailSender mailSender, UserService userService) {
    this.mailSender = mailSender;
    this.userService = userService;
  }
  
5)
we are going to look into message source. How to read messages from properties files.
Attributes class which is used for displaying messages and with locale, needs to be injected with a constructor
We are going to look at messages.properties which is used for saving messages.
<filename>_<language_code>_<country_code>.properties
example: messages_SV_SE.properties

6)
We are going to take a look at mail config. How to send mails with spring boot. 
Open mail config class and check the properties we have defined with attributes. 
Check the equivalent properties with the ones in application.properties. 

7)
implement the void send method using JavaMailSender
In SmtpMailSender class

8)
we are going to implement the sign up page with get requests and post requests. 
using the @RequestMapping(value = "/signup", method = RequestMethod.GET)

9)
a)
Implement the post request in sign up page
with @RequestMapping(value = "/signup", method = RequestMethod.POST)
input parameters:
@ModelAttribute("signupForm") @Valid SignUpForm signupForm, BindingResult result, HttpServletRequest request, RedirectAttributes redirectAttributes

it should collect a user from the sign up form class
store it in a user object , and use user service class, method signUp, to store new user
and send a mail to user that user has signed up

also implement:
result.hasErrors()

use Attributes class to save success message and other messages,"success", "signupSuccess"
Attributes.flash
Attributes.addAttribute, redirectAttributes, with user id

b)
In UserServiceImpl signup method, implement a method that validates email if it already exists in db. 
If it exists throw an appropriate error mesagse.
implement method  userExternalRepository.emailExists(form.getEmail().toLowerCase());
if exists throw an exception and handle error message
  

10)
Tomcat deployment

We are going to change the Application class so it runs with Spring Context Servlet and deployable to Tomcat

10.1)
extend this class extends SpringBootServletInitializer

10.2)
add this method to application class

 @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(Application.class);
  }
  
  
 
 Extra
 
 1) Go to src/test/java ApplicationTests and MyIntegrationTests class. Start writing junit tests
 
 http://docs.spring.io/spring/docs/current/spring-framework-reference/html/integration-testing.html
 https://dzone.com/articles/unit-and-integration-tests-in-spring-boot
 http://www.lucassaldanha.com/unit-and-integration-tests-in-spring-boot/
 
 2)
 Check the uncommented class SocialConfig how to connect to Facebook own plugin with spring and use their 
 user accounts. 
 
 3) 
 Add validation for email exists already in User Service signup method. Check in database return user id.
 throw an exception that user with email was found.
 	
 	 
 
 
 
