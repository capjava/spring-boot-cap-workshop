package spring.boot.cap.workshop.service;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import spring.boot.cap.workshop.Application;
import spring.boot.cap.workshop.TestData;
import spring.boot.cap.workshop.entity.Group;
import spring.boot.cap.workshop.entity.User;
import spring.boot.cap.workshop.entity.UserGroup;
import spring.boot.cap.workshop.forms.LoginForm;
import spring.boot.cap.workshop.forms.SignUpForm;
import spring.boot.cap.workshop.repository.UserGroupRepository;
import spring.boot.cap.workshop.repository.UserRepository;
import spring.boot.cap.workshop.repository.external.UserDao;
import spring.boot.cap.workshop.repository.external.UserExternalRepository;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(loader = org.springframework.boot.test.context.SpringBootContextLoader.class)
@ActiveProfiles("mysql")
public class UserServiceTest {

	// @Inject
	@Autowired
	@InjectMocks
	UserServiceImpl userService_mock;

	@Mock
	UserRepository userRepository_mock;

	@Mock
	UserGroupRepository userGroupRepository_mock;

	@Mock
	UserExternalRepository userExternalRepository_mock;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
		Mockito.reset(userRepository_mock, userExternalRepository_mock);
		userService_mock = new UserServiceImpl(userRepository_mock, userGroupRepository_mock);
		userExternalRepository_mock = mock(UserDao.class);
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	@Test
	public void findAllUsersShouldSucceed() {
		// given
		List<User> users = TestData.getUserData();

		given(userRepository_mock.findAll()).willReturn(users);

		// when
		List<User> result = userService_mock.findAllUsers();

		// Then
		verify(userRepository_mock, times(1)).findAll();
		assertThat(result.size(), equalTo(users.size()));
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	@Test
	public void findOneUserShouldSucceed() {
		// given
		List<User> users = TestData.getUserData();
		long expectedId = 1L;
		User expectedUser = users.get(0);
		given(userRepository_mock.getOne(expectedUser.getId())).willReturn(expectedUser);

		// when
		User result = userService_mock.find(expectedId);

		// Then
		verify(userRepository_mock, times(1)).getOne(expectedId);
		assertThat(result, equalTo(expectedUser));
	}

	@Test
	public void findByNameorEmail_ShouldSucceed() {
		userService_mock = new UserServiceImpl(userExternalRepository_mock);
		// given
		List<User> users = TestData.getUserData();
		User expectedUser = users.get(0);
		LoginForm form = TestData.getLoginForm();
		given(userExternalRepository_mock.findByNameOrEmail(TestData.NAME_VERIFIER)).willReturn(expectedUser);

		// when
		User result = userService_mock.findByNameorEmail(form);

		// Then
		verify(userExternalRepository_mock, times(1)).findByNameOrEmail(TestData.NAME_VERIFIER);
		assertThat(result, equalTo(expectedUser));
	}

	@Test
	public void findByNameorEmail_InputParam_Name_ShouldSucceed() {
		userService_mock = new UserServiceImpl(userExternalRepository_mock);
		// given
		List<User> users = TestData.getUserData();
		User expectedUser = users.get(0);
		given(userExternalRepository_mock.findByNameOrEmail(TestData.NAME_VERIFIER)).willReturn(expectedUser);

		// when
		User result = userService_mock.findByNameorEmail(TestData.NAME_VERIFIER);

		// Then
		verify(userExternalRepository_mock, times(1)).findByNameOrEmail(TestData.NAME_VERIFIER);
		assertThat(result, equalTo(expectedUser));
	}

	@Test
	public void findUserByNamePassword_ShouldSucceed() {
		userService_mock = new UserServiceImpl(userExternalRepository_mock);
		// given
		List<User> users = TestData.getUserData();
		User expectedUser = users.get(0);
		given(userExternalRepository_mock.findUserByNamePassword(TestData.NAME_VERIFIER, "")).willReturn(expectedUser);

		// when
		User result = userService_mock.findUserByNamePassword(TestData.NAME_VERIFIER, "");

		// Then
		verify(userExternalRepository_mock, times(1)).findUserByNamePassword(TestData.NAME_VERIFIER, "");
		assertThat(result, equalTo(expectedUser));
	}

	@Test
	public void userExists_ShouldSucceed() {
		userService_mock = new UserServiceImpl(userExternalRepository_mock);
		// given
		List<User> users = TestData.getUserData();
		User expectedUser = users.get(0);
		LoginForm form = TestData.getLoginForm();
		given(userExternalRepository_mock.findByNameOrEmail(TestData.NAME_VERIFIER)).willReturn(expectedUser);

		// when
		boolean result = userService_mock.userExists(form);

		// Then
		verify(userExternalRepository_mock, times(1)).findByNameOrEmail(TestData.NAME_VERIFIER);
		assertThat(result, equalTo(true));
	}

	@Test
	public void signUp_ShouldSucceed() {
		userService_mock = new UserServiceImpl(userRepository_mock, userExternalRepository_mock);
		// given
		List<User> users = TestData.getUserData();
		User expectedUser = users.get(0);
		SignUpForm form = TestData.getSignUpForm();
		given(userExternalRepository_mock.emailExists(TestData.EMAIL_VERIFIER)).willReturn(0L);
		Answer<User> answer = new Answer<User>() {
			@Override
			public User answer(InvocationOnMock invocation) {
				//User input = invocation.getArgument(0); //getArgumentAt(0, User.class);
				return expectedUser;
			}
		};
		given(userRepository_mock.saveAndFlush(any(User.class))).willAnswer(answer);

		// when
		User result = userService_mock.signUp(form);

		// Then
		verify(userExternalRepository_mock, times(1)).emailExists(TestData.EMAIL_VERIFIER);
		assertThat(result, equalTo(expectedUser));
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	@Test
	public void addUserWithGroup_shouldSucceed() {
		// given
		List<User> users = TestData.getUserData();
		long expectedId = 1L;
		User expectedUser = users.get(0);
		Group group = new Group();
		group.setName("ADMIN");
		UserGroup userGroup = new UserGroup();
		userGroup.setActivated(true);
		userGroup.setGroup(group);
		userGroup.setUser(expectedUser);
		//given(userRepository_mock.getOne(expectedUser.getId())).willReturn(expectedUser);
		Answer<UserGroup> answer = new Answer<UserGroup>() {
			@Override
			public UserGroup answer(InvocationOnMock invocation) {
				//UserGroup input = invocation.getArgumentAt(0, User.class);
				
				return userGroup;
			}
		};
		given(userGroupRepository_mock.save(any(UserGroup.class))).willAnswer(answer);

		// when
		User result = userService_mock.find(expectedId);
		// add user with group
		User saved = userService_mock.addUserWithGroups(expectedUser, group);

		// Then
		verify(userRepository_mock, times(1)).getOne(expectedId);
		assertThat(saved.getId(), equalTo(expectedUser.getId()));
	}

}
