package spring.boot.cap.workshop;


//import static org.mockito.BDDMockito.given;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.Properties;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.runners.MockitoJUnitRunner;
//import org.springframework.context.MessageSource;
//import org.springframework.context.support.ResourceBundleMessageSource;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
//import org.springframework.web.servlet.HandlerExceptionResolver;
//import org.springframework.web.servlet.ViewResolver;
//import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
//import org.springframework.web.servlet.view.InternalResourceViewResolver;
//import org.springframework.web.servlet.view.JstlView;
//
//
//import spring.boot.cap.workshop.controllers.LoginController;
//import spring.boot.cap.workshop.entity.User;
//import spring.boot.cap.workshop.forms.LoginForm;
//import spring.boot.cap.workshop.mail.MockMailSender;
//import spring.boot.cap.workshop.service.UserService;
//
//
//
//@ContextConfiguration
//@TestPropertySource("/test.properties")
//@RunWith(MockitoJUnitRunner.class)
//public class MyIntegrationTests {
//
//
//
//  private MockMvc mockMvc;
//
//  @Mock
//  private UserService userServiceMock;
//
//  @Mock
//  private MockMailSender mailMock;
//  
//  LoginController controller;
//
//  @Before
//  public void setUp() {
//    controller = new LoginController(mailMock, userServiceMock);
//    mockMvc = MockMvcBuilders.standaloneSetup(controller).setHandlerExceptionResolvers(exceptionResolver()).setValidator(validator()).setViewResolvers(viewResolver()).build();
//  }
//
//  private HandlerExceptionResolver exceptionResolver() {
//    SimpleMappingExceptionResolver exceptionResolver = new SimpleMappingExceptionResolver();
//
//    Properties exceptionMappings = new Properties();
//
//    exceptionMappings.put("net.petrikainulainen.spring.testmvc.todo.exception.TodoNotFoundException", "error/404");
//    exceptionMappings.put("java.lang.Exception", "error/error");
//    exceptionMappings.put("java.lang.RuntimeException", "error/error");
//
//    exceptionResolver.setExceptionMappings(exceptionMappings);
//
//    Properties statusCodes = new Properties();
//
//    statusCodes.put("error/404", "404");
//    statusCodes.put("error/error", "500");
//
//    exceptionResolver.setStatusCodes(statusCodes);
//
//    return exceptionResolver;
//  }
//
//  private MessageSource messageSource() {
//    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
//
//    messageSource.setBasename("i18n/messages");
//    messageSource.setUseCodeAsDefaultMessage(true);
//
//    return messageSource;
//  }
//
//  private LocalValidatorFactoryBean validator() {
//    return new LocalValidatorFactoryBean();
//  }
//
//  private ViewResolver viewResolver() {
//    InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//
//    viewResolver.setViewClass(JstlView.class);
//    viewResolver.setPrefix("/WEB-INF/jsp/");
//    viewResolver.setSuffix(".jsp");
//
//    return viewResolver;
//  }
//
//  public void testController() {
//
//  }
//
//  @Test
//  public void testHandleLogin() throws Exception {
//    MockMvc mockMvc = MockMvcBuilders.standaloneSetup(this.controller).build();
//  //  mockMvc.perform(post("/login").param("username", "john").param("password", "secret")).andExpect(status().isOk()).andExpect(request().sessionAttribute(LoginController.ACCOUNT_ATTRIBUTE, this.account))
//  //      .andExpect(redirectedUrl("/index.htm"));
//  }
//  
//  @Test
//  public void testExample() throws Exception {
//      given(this.userServiceMock.findByNameorEmail(new LoginForm("conny.pemfors@gmail.com","")))
//              .willReturn(new User(1L,"conny" ,"conny.pemfors@gmail.com","test"));
//      this.mockMvc.perform(get("/sboot/vehicle").accept(MediaType.TEXT_PLAIN))
//              .andExpect(status().isNotFound()).andExpect(content().string(""));
//  }
//
//  
//}