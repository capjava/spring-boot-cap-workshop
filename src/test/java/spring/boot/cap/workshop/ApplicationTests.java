package spring.boot.cap.workshop;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import spring.boot.cap.workshop.Application;

//@SpringApplicationConfiguration(classes = Application.class)
//@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
// @Import(MyTestsConfiguration.class)
public class ApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void applicationStarts() {
		Application.main(new String[] {});
	}

}
