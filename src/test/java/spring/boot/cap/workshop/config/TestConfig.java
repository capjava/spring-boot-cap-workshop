package spring.boot.cap.workshop.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = "spring.boot.cap.workshop.repository")
@PropertySource("jdbc.properties")
@EnableTransactionManagement
public class TestConfig {
    // ...
}
