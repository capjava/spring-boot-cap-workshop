package spring.boot.cap.workshop.rest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import spring.boot.cap.workshop.Application;
import spring.boot.cap.workshop.TestData;
import spring.boot.cap.workshop.config.TestConfig;
import spring.boot.cap.workshop.entity.User;
import spring.boot.cap.workshop.repository.UserGroupRepository;
import spring.boot.cap.workshop.repository.UserRepository;
import spring.boot.cap.workshop.repository.external.UserExternalRepository;
import spring.boot.cap.workshop.service.UserServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class, TestConfig.class }, webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(loader = org.springframework.boot.test.context.SpringBootContextLoader.class)
//@ActiveProfiles("mysql")
@ActiveProfiles("test")
public class UserRestControllerTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserRestControllerTest.class);

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Autowired
	DataSource dataSource;

	// @Mock
	// @InjectMocks
	// @MockBean
	// @Inject
	// @InjectMocks
	@Autowired
	@InjectMocks
	UserServiceImpl userService_mock;

	@Mock
	UserRepository userRepository;

	@Mock
	UserGroupRepository userGroupRepository;

	@Mock
	UserExternalRepository extUserRepository;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
		// Mockito.reset(userService_mock);
		// BasicAuthorizationInterceptor basicAuthInterceptor = new
		// BasicAuthorizationInterceptor("dmsp_user", "dmsp");
		// testRestTemplate.getRestTemplate().getInterceptors().add(basicAuthInterceptor);

		// using real database
		// JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		// jdbcTemplate.execute("TRUNCATE TABLE user");
		// jdbcTemplate.execute("INSERT INTO user (name,email,password) VALUES
		// ('t','t','t')");
		// jdbcTemplate.execute("INSERT INTO user (name,email,password) VALUES (
		// 't2','t2','t2')");

		// userService = new UserServiceImpl(userRepository);
		userService_mock = new UserServiceImpl(userRepository, userGroupRepository);
		// userService_mock = Mockito.mock(UserService.class);

	}

	@Test
	public void verifyControllerShouldSucceed() {

		// when
		ResponseEntity<String> responseEntity = testRestTemplate.getForEntity("/spring-boot-cap-workshop/users",
				String.class);

		// then
		assertThat(responseEntity.getStatusCode().value(), equalTo(200));
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	@Test
	public void findAllUsersShouldSucceed() {
		List<spring.boot.cap.workshop.domainmodel.User> users = TestData.getUserDataDomain();
		List<User> entities = TestData.getUserData();

		// when(service.getJdbcTemplate()).thenReturn(jdbcTemplate);
		when(userRepository.findAll()).thenReturn(entities);
		given(userService_mock.findAllUsers()).willReturn(entities);

		// when
		ResponseEntity<Collection> responseEntity = testRestTemplate
				.getForEntity("/spring-boot-cap-workshop/users/find/all", Collection.class);
		
		//ResponseEntity<String> exchange = testRestTemplate.exchange("/spring-boot-cap-workshop/users/find/all", HttpMethod.GET, null, String.class);
        
		Collection<spring.boot.cap.workshop.domainmodel.User> account = responseEntity.getBody();
		LOGGER.debug("found account" + account.size());

		// then
		assertThat(responseEntity.getStatusCode().value(), equalTo(200));
		// verify(userRepository, times(1)).findAll();
		// assertThat(account.size(), equalTo(users.size()));
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	@Test
	public void findUserByNameOrEmailShouldSucceed() {
		userService_mock = new UserServiceImpl(extUserRepository);
		List<User> users = TestData.getUserData();
		User expected = users.get(0);
		String testData = "test";
		given(userService_mock.findByNameorEmail(testData)).willReturn(expected);

		// Map<String, Object> params = new HashMap<>();
		// params.put("value", testData);
		String transactionUrl = "http://localhost:8080/api/v1/transactions";

		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("/spring-boot-cap-workshop/user/find")
				// Add query parameter
				.queryParam("value", testData);

		// when
		ResponseEntity<User> responseEntity = testRestTemplate
				.getForEntity("/spring-boot-cap-workshop/user/find/{value}", User.class, testData);
		User account = responseEntity.getBody();

		// then
		assertThat(responseEntity.getStatusCode().value(), equalTo(200));
		// verify(userRepository, times(1)).findAll();
		// assertThat(account.size(), equalTo(users.size()));
	}

}