package spring.boot.cap.workshop;

import java.util.ArrayList;
import java.util.List;

import spring.boot.cap.workshop.domainmodel.enums.UserAccountType;
import spring.boot.cap.workshop.entity.User;
import spring.boot.cap.workshop.forms.LoginForm;
import spring.boot.cap.workshop.forms.SignUpForm;

public class TestData {

	public static final String NAME_VERIFIER = "t_test";
	public static final String EMAIL_VERIFIER = "t@gmail.com";
	public static final String PWD_VERIFIER = "t_pwd";

	public static List<User> getUserData() {
		List<User> users = new ArrayList<>();
		User user = new User(1, EMAIL_VERIFIER, NAME_VERIFIER, "password");
		users.add(user);
		return users;
	}
	
	public static List<spring.boot.cap.workshop.domainmodel.User> getUserDataDomain() {
		List<spring.boot.cap.workshop.domainmodel.User> users = new ArrayList<>();
		spring.boot.cap.workshop.domainmodel.User user = new spring.boot.cap.workshop.domainmodel.User();
		users.add(user);
		return users;
	}

	public static LoginForm getLoginForm() {
		LoginForm form = new LoginForm();
		form.setUserLoginName(TestData.NAME_VERIFIER);
		return form;
	}

	public static SignUpForm getSignUpForm() {
		SignUpForm form = new SignUpForm();
		form.setName(NAME_VERIFIER);
		form.setEmail(EMAIL_VERIFIER);
		form.setPassword(PWD_VERIFIER);
		form.setPassword2(PWD_VERIFIER);

		return form;
	}
}