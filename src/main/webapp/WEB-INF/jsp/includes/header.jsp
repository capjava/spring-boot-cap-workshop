<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Up and running with Spring Framework quickly</title>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<link href="https://fonts.googleapis.com/css?family=Play:700"
	rel="stylesheet">

<link type="text/css"
	href="${contextPath}/public/lib/bootstrap/css/bootstrap.css"
	rel="stylesheet" />
<link type="text/css"
	href="${contextPath}/public/lib/bootstrap/css/bootstrap-theme.css"
	rel="stylesheet" />

<link type="text/css" href="${contextPath}/public/css/styles.css"
	rel="stylesheet" />


<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript"
	src="${contextPath}/public/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${contextPath}/public/lib/bootstrap/js/npm.js"></script>

</head>
<body>

	<div class="container">

		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
						aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Brand</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
						<li><a href="#">link</a></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-haspopup="true"
							aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul></li>
					</ul>
					<form class="navbar-form navbar-left">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search">
						</div>
						<button type="submit" class="btn btn-default">Search</button>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="<c:url value='signup' />"><span
								class="glyphicon glyphicon-list-alt"></span> <spring:message
									code="signup.navbar"></spring:message></a></li>

						<li><a href="<c:url value='login' />"><span
								class="glyphicon glyphicon-user"></span> <spring:message
									code="login.navbar"></spring:message></a></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-haspopup="true"
							aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Separated link</a></li>
							</ul></li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->

		</nav>
		<div class="st-locales">
			<div class="st-locales-en"><a href="?appLocale=en_US"><spring:message
					code='locales.english.title' /> </a>  </div>  <div class="st-locales-se"> <a href="?appLocale=sv_SE"><spring:message
					code='locales.swedish.title' /> </a> </div>
		</div>
		<div class="logged-in">
			<%@include file="logOut.jsp"%>
		</div>

		<div class="errorMessages">
			<c:if test="${not empty flashMessage}">
				<div class="row" id="error-container">
					<div class="span12">
						<div class="alert alert-${flashProperty}">
							<button type="button" class="close"></button>
							${flashMessage}
						</div>
					</div>
				</div>

			</c:if>

			<c:if test="${not empty flashErrorMessage}">
				<div class="row" id="error-container">
					<div class="span12">
						<div class="alert alert-danger fade in">
							<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong>
							${flashErrorMessage}
						</div>
					</div>
				</div>
			</c:if>

			<c:if test="${not empty errorMessage}">
				<div class="row" id="error-container">
					<div class="span12">
						<div class="alert alert-danger fade in">
							<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong>
							${errorMessage}
						</div>
					</div>
				</div>
			</c:if>
		</div>