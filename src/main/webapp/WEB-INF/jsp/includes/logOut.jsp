<c:if test="${not empty sessionScope.loggedUser}">
	<div class="row" id="logged-in-row">
		<div class="span12">
			<p class="left-pos">
			<h5 class="panel loggedin"><spring:message code='logout.loggedin' /> ${ sessionScope.loggedUser.name}</h5>
			</p>
			<p class="right-pos">
				<a href="logout" class="btn btn-info btn-lg"> <span
					class="glyphicon glyphicon-log-out"></span> <spring:message code='logout.button' />
				</a>
			</p>
		</div>
	</div>
</c:if>