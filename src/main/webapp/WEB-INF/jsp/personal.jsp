
<%@include file="includes/header.jsp"%>


<div class="panel-body border-frame ">
	<div class="h3">
		<spring:message code="personalPage" />
	</div>
	<form:form commandName="personalModel" id="personalForm" role="form"	action="personal" method="post">	
	<form:errors />


		<div class="form-group">
			<form:label path="name"><spring:message code='personal.name'/></form:label>
			<form:input path="name" class="form-control" placeholder="Enter name"></form:input>
			<form:errors path="name" cssClass="errorMessage"></form:errors>
			<p class="help-block">Enter your name</p>
		</div>

		<div class="form-group">
			<form:label path="email"><spring:message code='personal.email'/></form:label>
			<form:input path="email" type="email" class="form-control"
				placeholder="Enter email"></form:input>
			<form:errors path="email" cssClass="errorMessage"></form:errors>
			<p class="help-block">Enter unique email</p>
		</div>

		<div class="form-group">
			<form:label path="password"><spring:message code='personal.password'/></form:label>
			<form:password path="password" class="form-control"
				placeholder="Enter password"></form:password>
			<form:errors path="password" cssClass="errorMessage"></form:errors>
			<p class="help-block">Enter password</p>
		</div>

		<button type="submit" class="btn btn-primary btn-lg pd-size-smaller"><spring:message code='personal.submit'/></button>

	</form:form>
</div>

<%@include file="includes/footer.jsp"%>