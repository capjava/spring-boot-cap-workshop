<%@include file="includes/header.jsp"%>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">
			<spring:message code="login.title" />
		</h3>
	</div>
	<div class="panel-body">
		<form:form modelAttribute="loginForm" role="form" action="login"
			method="post">
			<form:errors />


			<div class="form-group">
				<form:label path="userLoginName">
					<spring:message code='login.name' />
				</form:label>
				<form:input path="userLoginName" class="form-control"
					placeholder="Enter name"></form:input>
				<form:errors path="userLoginName" cssClass="errorMessage"></form:errors>
				<p class="help-block">Enter your name or email</p>
			</div>

			<div class="form-group">
				<form:label path="password"><spring:message code='login.password'/></form:label>
				<form:password path="password" class="form-control"
					placeholder="Enter password"></form:password>
				<form:errors path="password" cssClass="errorMessage"></form:errors>
				<p class="help-block">Enter password</p>
			</div>


			<button type="submit" class="btn btn-default"><spring:message code='login.submit'/></button>

		</form:form>

	</div>
</div>
<%@include file="includes/footer.jsp"%>