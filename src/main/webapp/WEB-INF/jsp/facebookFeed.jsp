
<%@include file="includes/header.jsp"%>

<div style="width: 50%;" class="containerDiv" align=left>
	Facebook feeds
	<div class="rowDivHeader">
		<div class="cellDivHeader  w-title-large">
			<h5 align=center class="font-layout">Description
		</div>

	</div>

	<c:forEach var="feed" items="${feeds}">
		<div id="rowDiv" class="rowDiv">
			<div class="cellDiv w-title-large">${feed.description}</div>
		</div>
	</c:forEach>
</div>

<%@include file="includes/footer.jsp"%>