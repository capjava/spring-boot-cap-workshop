
<%@include file="includes/header.jsp"%>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">
			<spring:message code="signup.title" />
		</h3>
	</div>
	<div class="panel-body">
		<form:form modelAttribute="signupForm" role="form" action="signup" method="post">
			<form:errors />
			<div class="form-group">
				<form:label path="email"><spring:message code="signup.email" /></form:label>
				<form:input path="email" type="email" class="form-control" placeholder="Enter email"></form:input>
				<form:errors  path="email" cssClass="errorMessage"></form:errors>
				<p class="help-block">Enter unique email</p>
			</div>

			<div class="form-group">
				<form:label path="name"><spring:message code="signup.name" /></form:label>
				<form:input path="name" class="form-control" placeholder="Enter name"></form:input>
				<form:errors  path="name" cssClass="errorMessage"></form:errors>
				<p class="help-block">Enter your name</p>
			</div>

			<div class="form-group">
				<form:label path="password"><spring:message code="signup.password" /></form:label>
				<form:password path="password" class="form-control" placeholder="Enter password"></form:password>
				<form:errors  path="password" cssClass="errorMessage"></form:errors>
				<p class="help-block">Enter password</p>
			</div>

			<div class="form-group">
				<form:label path="password2"><spring:message code="signup.retype.password" /></form:label>
				<form:password path="password2" class="form-control"	placeholder="Enter password"></form:password>
				<form:errors  path="password2" cssClass="errorMessage"></form:errors>
				<p class="help-block">Re enter password</p>
			</div>
			
			<button type="submit" class="btn btn-default" ><spring:message code="signup.submit" /></button>

		</form:form>
	</div>
</div>
<%@include file="includes/footer.jsp"%>