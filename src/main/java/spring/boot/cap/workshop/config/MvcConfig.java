package spring.boot.cap.workshop.config;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import spring.boot.cap.workshop.util.StringUtils;
/***
 * 
 * @author cpemfors
 *  2016-09-18
 */

//add necessary annotations
//inherit correct Spring 4 MVC adapter

//#TODO
@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter{
	
  @Value("${locales.default.language}")
  private String language;

  @Value("${locales.default.country}")
  private String country;

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController("/").setViewName("home");
  }

  @Bean
  public LocaleResolver localeResolver() {
    SessionLocaleResolver localeResolver = new SessionLocaleResolver();
    if(StringUtils.hasValue(language) && StringUtils.hasValue(country) ){
      localeResolver.setDefaultLocale(new Locale(language, country));
    }
    else{
      localeResolver.setDefaultLocale(new Locale("en", "US"));
    }
    
    return localeResolver;
  }
  
  @Override
  public void addInterceptors(InterceptorRegistry registry) {
      LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
      interceptor.setParamName("appLocale");
      registry.addInterceptor(interceptor);
  } 
  

}
