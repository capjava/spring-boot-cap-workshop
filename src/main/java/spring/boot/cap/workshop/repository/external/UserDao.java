package spring.boot.cap.workshop.repository.external;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import spring.boot.cap.workshop.entity.User;


/***
 * 
 * @author cpemfors 2016-09-18
 */
@Repository("userExternalRepository")
@Transactional
public class UserDao implements UserExternalRepository<User, Long> {

  @PersistenceContext
  private EntityManager entityManager;

  @Autowired
  public UserDao(EntityManager entityManager) {
    this.entityManager = entityManager;
  }


  @Override
  public User findByNameOrEmail(String value) {
    try {

      final Query q = entityManager.createNamedQuery("findUserByNameOrEmail", User.class);
      q.setParameter(1, value);
      final User result = (User) q.getSingleResult();
      if (result == null) {
        return new User();
      }
      return result;
    } catch (NoResultException e) {
      return new User();
    }

  }
  
  @Override
  public List<User> findAllUsers() {
    try {

      final Query q = entityManager.createNamedQuery("findAllUsers", User.class);
      return q.getResultList();
    } catch (NoResultException e) {
      return Collections.emptyList();
    }

  }

  @Override
  public User findUserByNamePassword(String name, String password) {
    try {

      final Query q = entityManager.createNamedQuery("findUserByNamePassword", User.class);
      q.setParameter("userName", name);
      q.setParameter("password", password);
      final User result = (User) q.getSingleResult();
      if (result == null) {
        return new User();
      }
      return result;

    } catch (NoResultException e) {
      return new User();
    }

  }



  @Override
  public long emailExists(String email) {
    try {
      final Query q = entityManager.createNamedQuery("findUserByEmail", User.class);
      q.setParameter(1, email);
      final User result = (User) q.setMaxResults(1).getSingleResult();
      if (result == null) {
        return 0L;
      }
      return result.getId();

    } catch (NoResultException e) {
      return 0L;
    } catch (NonUniqueResultException e) {
      throw new spring.boot.cap.workshop.exceptions.EntityValidationException("Too many user with same email");
    }
  }

  @Override
  public boolean exists(String email) {
    return emailExists(email) > 0;
  }
}
