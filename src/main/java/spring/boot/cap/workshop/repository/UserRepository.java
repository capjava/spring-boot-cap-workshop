package spring.boot.cap.workshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.boot.cap.workshop.entity.User;


/***
 * 
 * @author cpemfors
 *  2016-09-18
 */
public interface UserRepository extends JpaRepository<User,Long>{

	
}
