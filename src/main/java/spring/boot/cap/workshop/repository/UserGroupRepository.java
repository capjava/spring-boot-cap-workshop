package spring.boot.cap.workshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.boot.cap.workshop.entity.User;
import spring.boot.cap.workshop.entity.UserGroup;

public interface UserGroupRepository  extends JpaRepository<UserGroup,Long>{
	
	
}
