package spring.boot.cap.workshop.repository.external;

import java.util.List;

/***
 * 
 * @author cpemfors 2016-09-18
 */
public interface UserExternalRepository<User, Long> /* extends UserRepository */ {

	User findByNameOrEmail(final String value);

	User findUserByNamePassword(final String name, final String password);

	long emailExists(String email);

	boolean exists(String email);

	List<User> findAllUsers();
}
