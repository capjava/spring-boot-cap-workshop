package spring.boot.cap.workshop.util;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import spring.boot.cap.workshop.controllers.RootController;

/***
 * 
 * @author cpemfors 2016-09-18
 */
@Component
// inject message source with using constructor
public class Attributes {

	protected static final Logger LOGGER = LoggerFactory.getLogger(Attributes.class);

	private static MessageSource messageSource;

	// #TODO
	@Autowired
	public Attributes(MessageSource messageSource) {
		LOGGER.info(System.getProperty("user.country"));
		LOGGER.info(System.getProperty("user.language"));
		this.messageSource = messageSource;
	}

	public static String getMessage(String key, Object... args) {
		return messageSource.getMessage(key, args, Locale.getDefault());
	}

	public static void flash(RedirectAttributes redirectAttributes, String property, String messageKey) {
		redirectAttributes.addFlashAttribute("flashProperty", property);
		redirectAttributes.addFlashAttribute("flashMessage", Attributes.getMessage(messageKey));
	}

	public static void flash(RedirectAttributes redirectAttributes, String messageKey) {
		redirectAttributes.addFlashAttribute(messageKey, Attributes.getMessage(messageKey));
	}

	public static void addAttribute(RedirectAttributes redirectAttributes, String key, String storeValue) {
		redirectAttributes.addFlashAttribute(key, storeValue);
	}

	public static String getSpecifiedMessage(String key) {
		return Attributes.getMessage(key);
	}

}
