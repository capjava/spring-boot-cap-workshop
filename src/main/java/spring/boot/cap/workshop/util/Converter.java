package spring.boot.cap.workshop.util;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import spring.boot.cap.workshop.domainmodel.Group;
import spring.boot.cap.workshop.domainmodel.UserGroup;

import spring.boot.cap.workshop.entity.User;

@Component
public class Converter {

	public spring.boot.cap.workshop.entity.User convertDomain(spring.boot.cap.workshop.domainmodel.User user) {
		spring.boot.cap.workshop.entity.User entity = new User();
		entity.setEmail(user.getEmail());
		entity.setName(user.getName());
		entity.setPassword(user.getPassword());
		entity.setEncryptedPassword(user.getPassword());
		List<spring.boot.cap.workshop.entity.UserGroup> groups = new ArrayList<>();
		user.getUserGroup().forEach( e -> {
			convertDomainUserGroups(entity, groups);
		});
		entity.setUserGroups(groups.stream().collect(java.util.stream.Collectors.toSet()));
		return entity;
	}

	private void convertDomainUserGroups(spring.boot.cap.workshop.entity.User entity,
			List<spring.boot.cap.workshop.entity.UserGroup> groups) {
		Group group = new Group();
		spring.boot.cap.workshop.entity.Group entityGroup = new spring.boot.cap.workshop.entity.Group();
		group.setId(entityGroup.getId());
		group.setName(entityGroup.getName());
		spring.boot.cap.workshop.entity.UserGroup userGroup = new spring.boot.cap.workshop.entity.UserGroup();
		userGroup.setGroup(entityGroup);
		userGroup.setUser(entity);
		groups.add(userGroup);
	}
	
	public List<spring.boot.cap.workshop.entity.User> convertDomain(List<spring.boot.cap.workshop.domainmodel.User> users) {
		List<spring.boot.cap.workshop.entity.User> domains = new ArrayList<>();
		users.forEach(e -> domains.add(convertDomain(e)));
		return domains;
	}
	
	public List<spring.boot.cap.workshop.domainmodel.User> convertEntity(List<spring.boot.cap.workshop.entity.User> users) {
		List<spring.boot.cap.workshop.domainmodel.User> domains = new ArrayList<>();
		users.forEach(e -> domains.add(convert(e)));
		return domains;
	}
	
	public spring.boot.cap.workshop.domainmodel.User  convert(spring.boot.cap.workshop.entity.User user) {
		spring.boot.cap.workshop.domainmodel.User domainUser = new spring.boot.cap.workshop.domainmodel.User();
		domainUser.setEmail(user.getEmail());

		domainUser.setName(user.getName());
		domainUser.setPassword(user.getPassword());
		List<UserGroup> groups = new ArrayList<>();
		user.getUserGroups().forEach( e -> {
			Group group = new Group();
			spring.boot.cap.workshop.entity.Group entityGroup = e.getGroup();
			group.setId(entityGroup.getId());
			group.setName(entityGroup.getName());
			UserGroup userGroup = new UserGroup();
			userGroup.setGroup(group);
			userGroup.setUser(domainUser);
			groups.add(userGroup);
		});
		domainUser.setUserGroup(groups);
		return domainUser;
	}
}
