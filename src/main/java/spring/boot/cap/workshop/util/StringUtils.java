package spring.boot.cap.workshop.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

  private static String EMAIL_PATTERN = "[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

  public static boolean isAnEmailField(String value) {
    return Pattern.matches(EMAIL_PATTERN, value);
  }

  public static boolean hasValue(String value){
    return  value != null && !value.trim().isEmpty();
  }
  
}
