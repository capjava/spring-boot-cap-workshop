package spring.boot.cap.workshop.exceptions;




public class EntityValidationException extends RuntimeException {

  private static final long serialVersionUID = -47444141421421711L;

  private String message = "This is an exception..";

  public EntityValidationException(String message) {

    this.message = message;

  }

  public String getMessage() {

    return message;

  }

  public void setMessage(String message) {

    this.message = message;

  }

}
