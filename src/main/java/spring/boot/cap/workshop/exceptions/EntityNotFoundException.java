package spring.boot.cap.workshop.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
class EntityNotFoundException extends RuntimeException {

	public EntityNotFoundException(String userId) {
		super("could not find user '" + userId + "'.");
	}
}