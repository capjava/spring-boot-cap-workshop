package spring.boot.cap.workshop.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.xml.wss.impl.misc.Base64;

/**
 * @author conpem
 * @realname Conny Pemfors
 * @version $Revision: 1.0 $
 */

public final class PasswordService {
  private static final Logger LOGGER = LoggerFactory.getLogger("errorslog");

  private static PasswordService instance;

  /**
   * Method getInstance.
   * 
   * @return PasswordService
   */
  public static synchronized PasswordService getInstance() // step 1
  {
    if (instance == null) {
      return new PasswordService();
    } else {
      return instance;
    }
  }

  private PasswordService() {}

  /**
   * Method encrypt.
   * 
   * @param plaintext String
   * 
   * 
   * 
   * 
   * @return String * @throws SystemUnavailableException * @throws SystemUnavailableException
   *         * @throws SystemUnavailableException * @throws SystemUnavailableException
   */
  public synchronized String encrypt(final String plaintext) throws RuntimeException {

    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("SHA"); // step 2
      md.update(plaintext.getBytes("UTF-8")); // step 3
    } catch (final NoSuchAlgorithmException | UnsupportedEncodingException e) {
      LOGGER.error(e.getMessage(), e);
      throw new RuntimeException(e.getMessage());
    }

    final byte raw[] = md.digest(); // step 4
    final String hash = Base64.encode(raw); // step 5
    return hash; // step 6
  }

  public static String hashedPassword(final String password) {
    final PasswordService service = PasswordService.getInstance();
    final String hashedPassword = service.encrypt(password);
    return hashedPassword;
  }
}
