package spring.boot.cap.workshop.domainmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;


//@JsonFormat(shape = JsonFormat.Shape.OBJECT)
//public enum UserGroups {
//	
//	ADMIN("Admin",UserGroupType.S004),
//	USER("User",UserGroupType.S003),
//	RESOURCE("Resource",UserGroupType.S002);
//	
//	private String name;
//	private UserGroupType userGroupType;
//	
//	
//
//	UserGroups(String name,  UserGroupType userGroupType){
//		this.name = name;
//		this.userGroupType = userGroupType;
//	}
//	
//	@JsonValue
//	public String getName() {
//		return name;
//	}
//	
//	@JsonValue
//	public UserGroupType getUserGroupType() {
//		return userGroupType;
//	}
//}

public class UserGroup {
	
	
	private User user;
	
	private Group group;
	
	// additional fields
	private Boolean activated;
	
	private Date registeredDate;

	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public Date getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}
	
}
