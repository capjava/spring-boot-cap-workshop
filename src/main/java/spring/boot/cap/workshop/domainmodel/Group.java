package spring.boot.cap.workshop.domainmodel;

import java.util.ArrayList;
import java.util.List;

import spring.boot.cap.workshop.entity.UserGroup;

public class Group {

	private long id;
	
	private String name;

	private List<UserGroup> userGroups = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}

	
}
