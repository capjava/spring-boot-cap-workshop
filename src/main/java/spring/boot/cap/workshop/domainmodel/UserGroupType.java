package spring.boot.cap.workshop.domainmodel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum UserGroupType {

	S001("S001", "Student"), S002("S002", "Course"), S003("S003", "Center"), S004("S004", "Admin");

	private String id;
	private String name;

	UserGroupType(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	@JsonValue
	public String getId() {
		return id;
	}
	@JsonValue
	public String getName() {
		return name;
	}

}
