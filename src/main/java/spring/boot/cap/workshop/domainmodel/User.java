package spring.boot.cap.workshop.domainmodel;

import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.AllArgsConstructor;
import spring.boot.cap.workshop.domainmodel.enums.UserAccountType;

@AllArgsConstructor
public class User {

	private String email;

	private String name;

	private String password;

	private List<UserGroup> userGroups;

	@Enumerated(EnumType.ORDINAL)
	private UserAccountType userAccountType;

	public UserAccountType getUserAccountType() {
		return userAccountType;
	}

	public void setUserAccountType(UserAccountType userAccountType) {
		this.userAccountType = userAccountType;
	}

	public void setUserAccountType(String userAccountType) {
		this.userAccountType = UserAccountType.byName(userAccountType);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<UserGroup> getUserGroup() {
		return userGroups;
	}

	public void setUserGroup(List<UserGroup> userGroup) {
		this.userGroups = userGroup;
	}

}
