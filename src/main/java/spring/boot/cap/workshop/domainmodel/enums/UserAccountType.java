package spring.boot.cap.workshop.domainmodel.enums;

import java.util.Arrays;

public enum UserAccountType {

	COMPANY(1, "Company", ""), ORGANISATION(2, "Organisation", ""), PRIVATE(3, "Private", "");

	private int id = 0;

	// @JsonProperty("name")
	private String name;

	// @JsonProperty("description")
	private String description;

	UserAccountType(int id, String name, String desc) {
		this.id = id;
		this.name = name;
		this.description = desc;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public static UserAccountType byName(String name) {

		return Arrays.asList(UserAccountType.values()).stream().filter(e -> e.name() == name).findFirst()
				.orElseThrow(IllegalArgumentException::new);
	}
}
