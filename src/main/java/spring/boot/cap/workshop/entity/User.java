package spring.boot.cap.workshop.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

import spring.boot.cap.workshop.security.PasswordService;

/***
 * 
 * @author cpemfors 2016-09-18
 */
@NamedQueries({ @NamedQuery(name = "findAllUsers", query = "SELECT u FROM User u order by u.id asc "),
		@NamedQuery(name = "findUserLikeName", query = "SELECT u FROM User u WHERE u.name LIKE ?1 "),
		@NamedQuery(name = "findUserByName", query = "SELECT u FROM User u WHERE LOWER(u.name) = ?1 "),
		@NamedQuery(name = "findUserByNameOrEmail", query = "SELECT u FROM User u WHERE  LOWER(u.name) = ?1 or LOWER(u.email) = ?1  "),
		@NamedQuery(name = "findUserByNamePassword", query = "SELECT u FROM User u WHERE LOWER(u.name) = :userName AND u.password = :password "),
		@NamedQuery(name = "findUserByNameOrEmailAndPassword", query = "SELECT u FROM User u WHERE ( LOWER(u.name) = :userName OR LOWER(u.email) = :userName  ) AND u.password = :password "),
		@NamedQuery(name = "findUserByEmail", query = "SELECT u FROM User u WHERE  LOWER(u.email) = ?1  ")

})
@Entity
@XmlRootElement
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "email", nullable = false)
	private String email;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "password", nullable = false, length = 255)
	@JsonIgnore
	private String password;
	
	@OneToMany(mappedBy = "user")
	private Set<UserGroup> userGroups = new HashSet<UserGroup>();


	public User() {
	}

	public User(long id, String email, String name, String password) {
		this.id = id;
		this.email = email;
		this.name = name;
		this.password = password;
	}

	public Long getId() {
		if (id == null) {
			return 0L;
		}
		return id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEncryptedPassword(String password) {
		if (password != null && !password.isEmpty()) {
			createPassword(password);
		}
	}

	public void createPassword(final String password) {
		final PasswordService service = PasswordService.getInstance();
		final String hashedPassword = service.encrypt(password);
		setPassword(hashedPassword);
	}
	
	public Set<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(Set<UserGroup> groups) {
		this.userGroups = groups;
	}
	
	public void addUserGroup(UserGroup userGroup) {
		this.userGroups.add(userGroup);
	}

}
