package spring.boot.cap.workshop.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
/***
 * 
 * @author cpemfors
 *  2016-09-18
 */
public class SignUpForm {

	@NotNull
	@Size(min=5,max=255)
	@Pattern(regexp="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",message="{emailPatternError}")
	private String email;

	@NotNull
	@Size(min=2,max=100)
	private String name;
	
	@NotNull
	@Size(min=6,max=45)
	private String password;
	
	@NotNull
	@Size(min=6,max=45,message="{passwordUnequalError}")
	private String password2;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

}
