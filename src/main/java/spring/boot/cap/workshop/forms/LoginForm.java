package spring.boot.cap.workshop.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/***
 * 
 * @author cpemfors 2016-09-18
 */
public class LoginForm {

  @NotNull
  @Size(min = 2, max = 255)
  private String userLoginName;


  @NotNull
  @Size(min = 6, max = 45)
  private String password;

  public LoginForm() {}

  public LoginForm(String login, String password) {
    this.userLoginName = login;
    this.password = password;
  }

  public String getUserLoginName() {
    return userLoginName;
  }

  public void setUserLoginName(String user) {
    this.userLoginName = user;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


}
