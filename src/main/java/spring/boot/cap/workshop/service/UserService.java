package spring.boot.cap.workshop.service;

import java.util.List;

import spring.boot.cap.workshop.entity.Group;
import spring.boot.cap.workshop.entity.User;
import spring.boot.cap.workshop.forms.LoginForm;
import spring.boot.cap.workshop.forms.SignUpForm;

/***
 * 
 * @author cpemfors
 *  2016-09-18
 */
public interface UserService {

	User signUp(SignUpForm form);

	User find(long id);

	User findByNameorEmail(LoginForm form);

	boolean userExists(LoginForm form);

	User findUserByNamePassword(String name, String password);

	List<User> findAllUsers();

	User findByNameorEmail(String value);
	
	User addUserWithGroups(User user, Group group);

	User addUser(User user);
}
