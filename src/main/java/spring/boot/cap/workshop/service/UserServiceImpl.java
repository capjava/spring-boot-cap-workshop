package spring.boot.cap.workshop.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import spring.boot.cap.workshop.entity.Group;
import spring.boot.cap.workshop.entity.User;
import spring.boot.cap.workshop.entity.UserGroup;
import spring.boot.cap.workshop.exceptions.EntityValidationException;
import spring.boot.cap.workshop.forms.LoginForm;
import spring.boot.cap.workshop.forms.SignUpForm;
import spring.boot.cap.workshop.repository.UserGroupRepository;
import spring.boot.cap.workshop.repository.UserRepository;
import spring.boot.cap.workshop.repository.external.UserExternalRepository;

/***
 * 
 * @author cpemfors 2016-09-18
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	private UserGroupRepository userGroupRepository;

	@Autowired
	@Qualifier("userExternalRepository")
	private UserExternalRepository userExternalRepository;

	@Autowired
	public UserServiceImpl(UserRepository userRepository, UserGroupRepository userGroupRepository) {
		this.userRepository = userRepository;
		this.userGroupRepository = userGroupRepository;
	}

	// for testing purpose
	public UserServiceImpl(UserExternalRepository userRepository) {
		this.userExternalRepository = userRepository;
	}

	// for testing purpose
	public UserServiceImpl(UserRepository userRepository, UserExternalRepository extUserRepository) {
		this.userRepository = userRepository;
		this.userExternalRepository = extUserRepository;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public User signUp(SignUpForm form) {

		long id = userExternalRepository.emailExists(form.getEmail().toLowerCase());
		if (id != 0L) {
			throw new EntityValidationException("User already exists");
		}
		User user = new User();
		user.setName(form.getName());
		user.setEmail(form.getEmail().toLowerCase());
		user.setEncryptedPassword(form.getPassword());
		User savedUser = userRepository.saveAndFlush(user);
		return savedUser;

	}

	@Override
	public User find(long id) {
		return userRepository.getOne(id);
	}

	@Override
	public List<User> findAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public User findByNameorEmail(LoginForm form) {
		return (User) userExternalRepository.findByNameOrEmail(form.getUserLoginName());
	}

	@Override
	public User findByNameorEmail(String value) {
		return (User) userExternalRepository.findByNameOrEmail(value);
	}

	@Override
	public boolean userExists(LoginForm form) {
		return ((User) userExternalRepository.findByNameOrEmail(form.getUserLoginName())).getId() > 0;
	}

	@Override
	public User findUserByNamePassword(String name, String password) {
		return (User) userExternalRepository.findUserByNamePassword(name, password);
	}

	@Override
	public User addUserWithGroups(User user, Group group) {
		UserGroup userGroup = new UserGroup();
		userGroup.setGroup(group);
		userGroup.setUser(user);
		userGroup.setActivated(true);
		userGroup.setRegisteredDate(new Date());
		UserGroup saved = userGroupRepository.save(userGroup);
		return saved.getUser();

	}

	@Override
	public User addUser(User user) {

		User saved = userRepository.saveAndFlush(user);
		return saved;

	}

}
