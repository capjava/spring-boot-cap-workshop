package spring.boot.cap.workshop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/***
 * 
 * @author cpemfors 2016-09-18
 */

// add necessary annotations
// implement static void main method

@Configuration
@ComponentScan(basePackages = { "spring.boot.cap.workshop","spring.boot.cap.workshop.controllers","spring.boot.cap.workshop.rest" } )
@EnableAutoConfiguration // (exclude={DataSourceAutoConfiguration.class})
//@EnableWebMvc  
@PropertySource("classpath:application.properties")
@EnableJpaRepositories(basePackages = { "spring.boot.cap.workshop.repository",
		"spring.boot.cap.workshop.repository.external" }, considerNestedRepositories = true)
@SpringBootApplication(scanBasePackages={"spring.boot.cap.workshop"})
@EnableTransactionManagement
public class Application {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(Application.class, args);
	}

	@Bean
	public ReloadableResourceBundleMessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages");
		messageSource.setCacheSeconds(3600); // refresh cache once per hour
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

}
