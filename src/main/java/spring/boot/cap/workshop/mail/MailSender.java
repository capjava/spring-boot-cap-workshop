package spring.boot.cap.workshop.mail;

import javax.mail.MessagingException;
/***
 * 
 * @author cpemfors
 *  2016-09-18
 */
public interface MailSender {

	public abstract void send(String to, String subject, String body) throws MessagingException;

}