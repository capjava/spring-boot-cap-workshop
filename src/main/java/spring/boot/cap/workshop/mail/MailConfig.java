package spring.boot.cap.workshop.mail;

import java.security.GeneralSecurityException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/***
 * 
 * @author cpemfors
 *  2016-09-18
 */
@Configuration
public class MailConfig {
	
	@Value("${mail.sender.host}")
	private String host;
	
	@Value("${smtp.authenticator.email}")
	private String username;
	
	@Value("${smtp.authenticator.password}")
	private String password;
	
	@Value("${smtp.authenticator.port}")
    private String port;

	@Bean
	@Profile("dev")
	public MailSender mockMailSender() {
		return new MockMailSender();
	}

	@Bean
	@Profile("!dev")
	public MailSender smtpMailSender() {
		SmtpMailSender mailSender = new SmtpMailSender();
		mailSender.setJavaMailSender(javaMailSender());
		return mailSender;
	}

	@Bean
	public JavaMailSender javaMailSender() {
		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		
		sender.setHost(host);
		sender.setSession(getMailSession());
		
		return sender;
	}

	private Session getMailSession() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.socketFactory.port", port);
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", false);
		
		props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.enable","true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.debug", "true");
        props.setProperty( "mail.imap.port", "993");
        props.setProperty( "mail.imap.socketFactory.port", "993");
        // POP3 provider
        props.setProperty( "mail.pop3.port", "995");
        props.setProperty( "mail.pop3.socketFactory.port", "995");
        
		
		return Session.getInstance(props, getAuthenticator());
	}

	

	private Authenticator getAuthenticator() {
		SmtpAuthenticator authenticator = new SmtpAuthenticator();
		authenticator.setUsername(username);
		authenticator.setPassword(password);
		return authenticator;
	}

}
