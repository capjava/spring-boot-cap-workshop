package spring.boot.cap.workshop.controllers;

import java.util.Map;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import spring.boot.cap.workshop.entity.User;
import spring.boot.cap.workshop.exceptions.EntityValidationException;
import spring.boot.cap.workshop.forms.SignUpForm;
import spring.boot.cap.workshop.mail.MailSender;
import spring.boot.cap.workshop.service.UserService;
import spring.boot.cap.workshop.util.Attributes;


/***
 * 
 * @author cpemfors 2016-09-18
 */
// add necessary annotation
// add mailsender with injection
// add user service class with correct injection
// implement get request for signup page
// implement post request for sign up page

@Controller
public class RootController {

  protected static final Logger LOGGER = LoggerFactory.getLogger(RootController.class);


  private MailSender mailSender;
  private UserService userService;

  @Autowired
  private ErrorAttributes errorAttributes;

  @Bean
  public AppErrorController appErrorController() {
    return new AppErrorController(errorAttributes);
  }

  @Autowired
  public RootController(MailSender mailSender, UserService userService) {
    this.mailSender = mailSender;
    this.userService = userService;
  }

  @RequestMapping("/")
  public String home() throws MessagingException {
    return "home";
  }

  @RequestMapping(value = "/signup", method = RequestMethod.GET)
  public String signup(Model model) throws MessagingException {
    model.addAttribute("signupForm", new SignUpForm());
    return "signup";

  }

  @RequestMapping(value = "/signup", method = RequestMethod.POST)
  public String signup(@ModelAttribute("signupForm") @Valid SignUpForm signupForm, BindingResult result, HttpServletRequest request, RedirectAttributes redirectAttributes) throws MessagingException {

    if (result.hasErrors()) {
      return "signup";
    }
    if (!signupForm.getPassword().trim().equals(signupForm.getPassword2().trim())) {
      return "signup";
    }
    try {

      User savedUser = userService.signUp(signupForm);
      Attributes.flash(redirectAttributes, "success", "signupSuccess");
      Attributes.addAttribute(redirectAttributes, "id", Long.toString(savedUser.getId()));
      HttpSession session = request.getSession();
      session.setAttribute("loggedUser", savedUser);

      mailSender.send(savedUser.getEmail(), "Signed up for application", "Signed up for application");

      return "redirect:/personal?id=" + savedUser.getId();

    }
   /* catch(EntityValidationException e){
      throw e;
    }*/
    catch (RuntimeException e) {
      LOGGER.error(e.getMessage());
      Attributes.flash(redirectAttributes, "flashErrorMessage");
      Attributes.addAttribute(redirectAttributes,"flashErrorMessage",e.getMessage());
      return "redirect:/signup";
    }

  }

  @RequestMapping(value = "/personal", method = RequestMethod.GET)
  public String displayPersonalPage(@RequestParam(value = "id", required = false) String id, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) {

    User loggedUser = (User) request.getSession().getAttribute("loggedUser");
    if(loggedUser == null){
        return "redirect:login";
    }
    if (redirectAttributes != null && redirectAttributes.containsAttribute("id")) {
      Map<String, ?> map = redirectAttributes.getFlashAttributes();
      id = (String) map.get("id");
    }
    if (id != null) {
      User user = userService.find(Long.valueOf(id));
      model.addAttribute("personalModel", user);
      model.addAttribute("userName", user.getName());
      return "personal";
    } else if (loggedUser != null) {
      User user = userService.find(Long.valueOf(loggedUser.getId()));
      model.addAttribute("personalModel", user);
      model.addAttribute("userName", user.getName());
      return "personal";
    } 
    return "redirect:login";
  }

  @ExceptionHandler(EntityValidationException.class)
  public ModelAndView handleCustomException(EntityValidationException ex) {
    LOGGER.info("Handling exception");
    ModelAndView model = new ModelAndView("error");
    model.addObject("exception", ex);
    return model;

  }
}
