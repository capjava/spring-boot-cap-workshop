package spring.boot.cap.workshop.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



import spring.boot.cap.workshop.entity.User;
import spring.boot.cap.workshop.forms.LoginForm;
import spring.boot.cap.workshop.mail.MailSender;
import spring.boot.cap.workshop.security.PasswordService;
import spring.boot.cap.workshop.service.UserService;
import spring.boot.cap.workshop.util.Attributes;




/**
 * @author conpem 2016-04-15
 * @realname Conny Pemfors
 * @version $Revision: 1.0 $
 */
@Controller("loginController")
public class LoginController {

  
  
  private MailSender mailSender;
  private UserService userService;

  @Autowired
  private ErrorAttributes errorAttributes;
  
 

  @Bean
  public AppErrorController appErrorController() {
    return new AppErrorController(errorAttributes);
  }

  @Autowired
  public LoginController(MailSender mailSender, UserService userService) {
    this.mailSender = mailSender;
    this.userService = userService;
  }


  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public String login(Model model) {
    model.addAttribute("loginForm", new LoginForm());
    return "login";
  }

  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public String login(@Valid @ModelAttribute("loginForm") LoginForm loginForm, BindingResult result, Model model, HttpServletRequest request) {

    if (result.hasErrors()) {
      return "login";
    }
    boolean exists = userService.userExists(loginForm);
    if (!exists) {
      model.addAttribute("errorMessage", Attributes.getSpecifiedMessage("loginError"));
      return "login";
    } else {

      String decrypted = PasswordService.hashedPassword(loginForm.getPassword());
      User user = userService.findUserByNamePassword(loginForm.getUserLoginName().toLowerCase(), decrypted);
      if (user.getId() > 0) {
        HttpSession session = request.getSession();
        session.setAttribute("loggedUser", user);
        return "redirect:personal?id=" + user.getId();
      } else {
        model.addAttribute("errorMessage", Attributes.getSpecifiedMessage("invalidPasswordError"));
        return "login";
      }

    }

  }
  
}
