package spring.boot.cap.workshop.rest;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.boot.cap.workshop.entity.Group;
import spring.boot.cap.workshop.entity.User;
import spring.boot.cap.workshop.service.UserService;
import spring.boot.cap.workshop.util.Converter;

@RestController
@RequestMapping("/spring-boot-cap-workshop") // ("/users")
public class UserRestController {

	private final UserService userService;
	private final Converter converter;

	@Autowired
	public UserRestController(UserService userService, Converter converter) {
		this.userService = userService;
		this.converter = converter;
	}

	@RequestMapping("/users")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/users/find/all", produces = "application/json")
	Collection<spring.boot.cap.workshop.domainmodel.User> findAllUsers() {
		List<User> users = userService.findAllUsers();
		return converter.convertEntity(users);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/user/find/{value}", produces = "application/json")
	User findUserByNameOrEmail(@PathVariable String value) {
		return userService.findByNameorEmail(value);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/user/create",produces = "application/json")
	User createUserWithGroups(@RequestBody spring.boot.cap.workshop.domainmodel.User user) {
		spring.boot.cap.workshop.entity.User entity = converter.convertDomain(user);
		return userService.addUser(entity);
	}
}

// @RestController
// @RequestMapping("/{userId}/bookmarks")
// class BookmarkRestController {
//
// private final BookmarkRepository bookmarkRepository;
//
// private final AccountRepository accountRepository;
//
// @Autowired
// BookmarkRestController(BookmarkRepository bookmarkRepository,
// AccountRepository accountRepository) {
// this.bookmarkRepository = bookmarkRepository;
// this.accountRepository = accountRepository;
// }
//
// @RequestMapping(method = RequestMethod.GET)
// Collection<Bookmark> readBookmarks(@PathVariable String userId) {
// this.validateUser(userId);
// return this.bookmarkRepository.findByAccountUsername(userId);
// }
//
// @RequestMapping(method = RequestMethod.POST)
// ResponseEntity<?> add(@PathVariable String userId, @RequestBody Bookmark
// input) {
// this.validateUser(userId);
//
// return this.accountRepository
// .findByUsername(userId)
// .map(account -> {
// Bookmark result = bookmarkRepository.save(new Bookmark(account,
// input.uri, input.description));
//
// URI location = ServletUriComponentsBuilder
// .fromCurrentRequest().path("/{id}")
// .buildAndExpand(result.getId()).toUri();
//
// return ResponseEntity.created(location).build();
// })
// .orElse(ResponseEntity.noContent().build());
//
// }
//
// @RequestMapping(method = RequestMethod.GET, value = "/{bookmarkId}")
// Bookmark readBookmark(@PathVariable String userId, @PathVariable Long
// bookmarkId) {
// this.validateUser(userId);
// return this.bookmarkRepository.findOne(bookmarkId);
// }
//
// private void validateUser(String userId) {
// this.accountRepository.findByUsername(userId).orElseThrow(
// () -> new UserNotFoundException(userId));
// }
// }