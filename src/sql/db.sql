
--create db
 CREATE DATABASE versatileteacher;
 
--create table user
CREATE TABLE versatileteacher.user (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL DEFAULT '',
  password VARCHAR(255) NOT NULL,
  email VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 20
AVG_ROW_LENGTH = 910
CHARACTER SET utf16
COLLATE utf16_unicode_ci;